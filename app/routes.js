module.exports = function(app, streams) {

  // GET home
  var index = function(req, res) {
    res.render('index', {
                          title: '智能终端视频通讯',
                          header: '智能终端视频通讯',
                          username: 'Username',
                          share: 'Share this link',
                          footer: 'pierre@chabardes.net',
                          id: req.params.id
                        });
  };

  // GET streams as JSON
  var displayStreams = function(req, res) {
    var streamList = streams.getStreams();
    // JSON exploit to clone streamList.public
    var data = (JSON.parse(JSON.stringify(streamList)));

    res.status(200).json(data);
  };

  var call = function(req,res){
    res.render('call',{id:req.params.id});
  };

  var test = function(req,res){
    res.render('test');
  };

  app.get('/streams.json', displayStreams);
  app.get('/', index);
  app.get('/:id', index);
  app.get('/call/:id', call);
  app.get('/test/stun',test);
};
