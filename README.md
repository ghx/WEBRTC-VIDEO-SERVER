
# 说明

> 部署说明：
> - 安装relib re-0.4.14.tar.gz 
> - 安装restund restund-0.4.11.tar.gz
> - 复制配置文件
>   - sudo cp etc/restund.* /etc/
>   - sudo ln -s /usr/local/lib/libre.so /usr/lib/libre.so
>   - 配置文件
>      - status_udp_addr         127.0.0.1
>      - status_udp_port         33000
>      - status_http_addr        127.0.0.1
>      - status_http_port        88080
> - 运行restund: restund -d /etc/restund.conf

```shell
udp listen: 127.0.0.1:3478
tcp listen: 127.0.0.1:3478
stat: module loaded
binding: module loaded
auth: module loaded (nonce_expiry=3600s)
turn: lifetime=600 ext=127.0.0.1 ext6=::1 bsz=512
filedb: module loaded (/etc/restund.auth)
syslog: module loaded facility=24
status: module loaded (udp=127.0.0.1:33000 http=127.0.0.1:2832)
```

- 三维端使用是 /call/:id 获取页面显示视频
- android端使用 /streams.json 获取ID数据



# ProjectRTC

## WebRTC Live Streaming

- Node.js server
- Desktop client
- [Android client](https://github.com/pchab/AndroidRTC)

The signaling part is done with [socket.io](socket.io).
The client is built with [angularjs](https://angularjs.org/).

## Install

It requires [node.js](http://nodejs.org/download/)

* git clone https://github.com/pchab/ProjectRTC.git
* cd ProjectRTC/
* npm install
* npm start

The server will run on port 3000.
You can test it in the (Chrome or Firefox) browser at localhost:3000.

## Author

- [Pierre Chabardes](mailto:pierre@chabardes.net): Looking for a developper ? I'm looking for a job !
